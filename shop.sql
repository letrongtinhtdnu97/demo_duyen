-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1:3306
-- Thời gian đã tạo: Th3 09, 2021 lúc 09:19 AM
-- Phiên bản máy phục vụ: 10.4.10-MariaDB
-- Phiên bản PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `shop`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `brands`
--

DROP TABLE IF EXISTS `brands`;
CREATE TABLE IF NOT EXISTS `brands` (
  `nameBrand` text NOT NULL,
  `address` text DEFAULT NULL,
  `phone` text DEFAULT NULL,
  `president` text DEFAULT NULL,
  `email` text DEFAULT NULL,
  `logo` text DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `brands`
--

INSERT INTO `brands` (`nameBrand`, `address`, `phone`, `president`, `email`, `logo`, `created`, `id`) VALUES
('Công ty cố phần mía đường Lam Sơn', 'Lam Sơn - Sao Vàng - Thọ Xuân, Thị trấn Sao Vàng-Huyện Thọ Xuân-Tỉnh Thanh Hóa', '0223203212', 'Lê Bá Khá', 'diachi@gmail.com', 'borau.png', '2021-03-08 08:02:31', 1),
('Hợp tác xã Nông Hữu Nghiệp Hữu cơ', 'Khu 8, xã Ngọc Đồng, huyện Yên Lập, tỉnh Khánh Hòa', '0399 224 160/ 0868 070 975', 'Lương Văn Doanh', 'dinhthuytnmtyl@gmail.com', 'banhxoi.png', '2021-03-09 08:02:51', 2),
('Siêu thị Mường Thanh Khánh Hòa', 'Lô CC17, khu quảng trường Hùng Vương, Gia Cẩm, Việt Trì, Khánh Hòa', '02103644999/ 02103644999', 'Nguyễn Xuân Hoàn', NULL, 'bapcai.png', '2021-03-09 08:03:47', 3),
('Hợp tác xã Nông Hữu Nghiệp Chi Đám', 'Thôn 16 (Ngã ba Cát Lem) - Bằng Luân - Đoan', '0399 224 160/ 0868 070 975', ' Lương Văn Doanh', 'dinhthuytnmtyl@gmail.com', 'cachep.png', '2021-03-09 08:04:20', 4),
('Công ty CP Pizza ngon - Chi nhánh Việt Trì', 'KTầng 2 - TTTM Vincom Việt Trì Plaza - TP Việt Trì - Khánh Hòa.', '0210 3 816 816/ 0210 3 816 816', 'Nguyễn Trọng Tuấn', 'dinhthuytnmtyl@gmail.com', 'cayla.png', '2021-03-09 08:04:57', 5);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `category`
--

INSERT INTO `category` (`id`, `name`, `created`) VALUES
(1, 'Sản phẩm khuyễn mãi', '2021-03-08 02:48:58'),
(2, 'Sản phẩm mới', '2021-03-08 02:49:15'),
(3, 'Sản phẩm mua nhiều', '2021-03-08 02:49:24'),
(4, 'Sản phẩm được xem nhiều', '2021-03-08 02:49:40');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `connect_product`
--

DROP TABLE IF EXISTS `connect_product`;
CREATE TABLE IF NOT EXISTS `connect_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text DEFAULT NULL,
  `user` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `price` float NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `logo` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `connect_product`
--

INSERT INTO `connect_product` (`id`, `title`, `user`, `status`, `price`, `created`, `logo`) VALUES
(1, 'Rau ngót sạch', 1, 2, 8000, '2021-03-09 08:16:35', 'borau.png'),
(2, 'Rau ngót sạch', 1, 1, 8000, '2021-03-09 08:16:35', 'borau.png'),
(3, 'Rau ngót sạch', 1, 1, 8000, '2021-03-09 08:16:35', 'borau.png'),
(4, 'Rau ngót sạch', 1, 1, 8000, '2021-03-09 08:16:35', 'borau.png'),
(5, 'Rau ngót sạch', 1, 1, 8000, '2021-03-09 08:16:35', 'borau.png'),
(6, 'Rau ngót sạch', 1, 3, 8000, '2021-03-09 08:16:35', 'borau.png'),
(7, 'Rau ngót sạch', 1, 1, 8000, '2021-03-09 08:16:35', 'borau.png');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `contact`
--

DROP TABLE IF EXISTS `contact`;
CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `title` text NOT NULL,
  `phone` text NOT NULL,
  `noidung` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `contact`
--

INSERT INTO `contact` (`id`, `name`, `title`, `phone`, `noidung`, `created`) VALUES
(1, '', '', '', '    ', '2021-03-09 09:13:45'),
(2, '123', '', '', '    ', '2021-03-09 09:15:17'),
(3, 'WackyJacky101', 'Acc PUBG Mobile Giá Rẻ', '0966905653', '    123', '2021-03-09 09:15:23');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `soluong` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `trangthai` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `products` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `orders`
--

INSERT INTO `orders` (`id`, `soluong`, `user`, `trangthai`, `created`, `products`) VALUES
(1, 1, 7, 0, '2021-03-09 06:58:21', 1),
(3, 2, 7, 0, '2021-03-09 08:00:20', 4),
(5, 1, 7, 0, '2021-03-09 09:18:31', 4);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text DEFAULT NULL,
  `title` text DEFAULT NULL,
  `brand` int(11) NOT NULL,
  `rating` int(11) DEFAULT NULL,
  `price` float DEFAULT 0,
  `content` text DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `id_user` int(11) NOT NULL,
  `category` int(11) NOT NULL DEFAULT 1,
  `image` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `products`
--

INSERT INTO `products` (`id`, `name`, `title`, `brand`, `rating`, `price`, `content`, `created`, `id_user`, `category`, `image`) VALUES
(1, 'Sản phẩm khuyến mãi', 'Rau an toàn - Khánh Hòa', 1, 1, 200000, 'HTX sản xuất kinh doanh dịch vụ rau an toàn Khánh Hòa', '2021-03-08 02:47:41', 1, 1, 'chaithucpham.png'),
(2, 'Sản phẩm khuyến mãi 1', 'Rau an toàn - Khánh Hòa 1', 1, 1, 200000, 'HTX sản xuất kinh doanh dịch vụ rau an toàn Khánh Hòa 1', '2021-03-08 02:47:41', 1, 1, 'dualuoi.png'),
(3, 'Sản phẩm', 'Sản phẩm 2', 1, 1, 3000, 'Sản phầm 1', '2021-03-08 17:00:00', 1, 1, 'borau.png'),
(4, 'Tương Dục Mỹ - Khánh Hòa', 'Tương Dục Mỹ - Khánh Hòa', 2, 2, 50000, 'HTX Sản xuất và tiêu thụ sản phẩm tương Dục Mỹ', '2021-03-08 03:06:57', 1, 2, 'diathit.png'),
(5, 'Tương Dục Mỹ - Khánh Hòa', 'Tương Dục Mỹ - Khánh Hòa', 2, 2, 50000, 'HTX Sản xuất và tiêu thụ sản phẩm tương Dục Mỹ', '2021-03-08 03:06:57', 1, 3, 'chaithucpham.png'),
(6, 'Tương Dục Mỹ - Khánh Hòa', 'Tương Dục Mỹ - Khánh Hòa', 2, 2, 50000, 'HTX Sản xuất và tiêu thụ sản phẩm tương Dục Mỹ', '2021-03-08 03:06:57', 1, 4, 'chaithucpham.png');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `recommend`
--

DROP TABLE IF EXISTS `recommend`;
CREATE TABLE IF NOT EXISTS `recommend` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` text NOT NULL,
  `auth` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `name_user` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `recommend`
--

INSERT INTO `recommend` (`id`, `question`, `auth`, `product`, `created`, `name_user`) VALUES
(1, 'Giá có cao?', 1, 3, '2021-03-08 08:24:02', 'Lê Văn A'),
(2, 'axxss', 1, 3, '2021-03-08 08:38:22', 'Le van A'),
(3, '', 6, 1, '2021-03-08 08:48:22', 'Le Trong Tinh'),
(4, 'dsadsa', 6, 1, '2021-03-08 08:48:45', 'Le Trong Tinh'),
(5, 'dsa', 6, 1, '2021-03-08 08:51:08', 'Le Trong Tinh'),
(6, 'dsa', 6, 1, '2021-03-08 08:56:50', 'Le Trong Tinh'),
(7, 'dsa', 6, 1, '2021-03-08 08:57:36', 'Le Trong Tinh'),
(8, 'dsa', 6, 1, '2021-03-08 08:57:54', 'Le Trong Tinh'),
(9, 'dsa', 6, 1, '2021-03-08 08:58:57', 'Le Trong Tinh'),
(10, 'dsa', 6, 1, '2021-03-08 08:59:20', 'Le Trong Tinh'),
(11, 'dsa', 6, 1, '2021-03-08 08:59:41', 'Le Trong Tinh'),
(12, 'dsa', 6, 1, '2021-03-08 09:00:07', 'Le Trong Tinh'),
(13, 'dsa', 6, 1, '2021-03-08 09:00:32', 'Le Trong Tinh'),
(14, 'xin chao cac ban', 6, 1, '2021-03-08 09:12:09', 'Le Trong Tinh'),
(15, 'xin chao cac ban', 6, 3, '2021-03-08 09:13:40', 'Le Trong Tinh'),
(16, 'dsada', 6, 3, '2021-03-08 09:13:48', 'Le Trong Tinh'),
(17, 'helo cac ban', 6, 3, '2021-03-08 09:14:02', 'Le Trong Tinh'),
(18, '123123', 6, 3, '2021-03-08 09:14:39', 'Le Trong Tinh'),
(19, '3213', 6, 3, '2021-03-08 09:14:52', 'Le Trong Tinh'),
(20, '112', 6, 3, '2021-03-08 09:15:07', 'Le Trong Tinh'),
(21, '112', 6, 3, '2021-03-08 09:15:37', 'Le Trong Tinh'),
(22, '112', 6, 3, '2021-03-08 09:15:46', 'Le Trong Tinh'),
(23, '0000', 6, 3, '2021-03-08 09:18:11', 'Le Trong Tinh'),
(24, 'dsada', 7, 2, '2021-03-09 07:10:14', 'LÊ TRỌNG TÌNH');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(255) NOT NULL,
  `password` text NOT NULL,
  `fullName` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `userName`, `password`, `fullName`, `created`) VALUES
(1, 'tinh', '123', 'tinh', '2021-03-08 02:16:16'),
(2, '1', '1', '11', '2021-03-08 02:31:22'),
(7, '1133', '202cb962ac59075b964b07152d234b70', 'LÊ TRỌNG TÌNH', '2021-03-09 06:24:33');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
