<?php 

   if(!isset($_SESSION)) 
   { 
       session_start(); 
   }
  include_once('./system/config.php');

  $id = $_SESSION['login'];
  if(isset($_GET['sp'])) {
    $idsp = $_GET['sp'];
    $id11 = $_POST['quantity'];
    // echo $idsp;
    // echo 'aaaaaaaaa';
    // echo $id11;
    $sql ="INSERT INTO `orders`(`soluong`, `user`, `trangthai`,`products`) VALUES ($id11,$id,0,$idsp)";
    $query= $dbh -> prepare($sql);
    $query-> execute();
    echo "<script type='text/javascript'> document.location = '?a=order'; </script>";
  }
  $sql0 ="SELECT * FROM orders 
  LEFT JOIN products ON orders.products = products.id WHERE orders.user = $id";

  if(isset($_GET['d'])) {
    $id1 = $_GET['d'];
    $sql ="DELETE FROM orders WHERE id=$id1";
    $query= $dbh -> prepare($sql);
    $query-> execute();
    echo "<script>alert('Xóa thành công đơn hàng');</script>";
    echo "<script type='text/javascript'> document.location = '?a=order'; </script>";
  }
  $query1= $dbh -> prepare($sql0);
  $query1-> execute();
  $order=$query1->fetchAll(PDO::FETCH_OBJ);
  $arrProcess = [
    '0' => 'Chờ xử lý',
    '1' => 'Đã giao hàng',
    '2' => 'Kết thúc',
    '3' => 'Kết thúc'
  ];
  $i = 0;
?>


<div class="container">
<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Tên sản phẩm</th>
      <th scope="col">Giá</th>
      <th scope="col">Số lượng</th>
      <th scope="col">Trạng thái</th>
      <th scope="col">Xử lý</th>
    </tr>
  </thead>
  <tbody>
      <?php 
        foreach($order as $key) {
          $i = $i + 1;
          ?>
          <tr>
            <th scope="row"><?= $i ?></th>
            <td><?= $key->title ?></td>
            <td><?= $key->price ?></td>
            <td><?= $key->soluong ?></td>
            <td>
            <?php
						switch ($key->trangthai) {
							case 0:
								echo $arrProcess[0];
								break;
							case 1:
								echo $arrProcess[1];
								break;
							case 2:
								echo $arrProcess[2];
								break;
							default:
								echo null;
								break;
						}
						?>
            </td>
            <td>
            <?php
						if ($key->trangthai == 0) {
						?>
							<a href="?a=order&d=<?= $key->id ?>" class="btn btn-primary">Xóa đơn</a>
						<?php
						} else {
						?>
							<a class="genric-btn danger">Đã xử lý</a>
						<?php
						}
						?>
            </td>
          </tr>
          <?php
        }
      ?>
  </tbody>
</table>
</div>