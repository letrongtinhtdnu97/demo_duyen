<?php 
    include 'system/brand.php';
  ?>
<div class="prod-faci-wrap">
    <div class="container">
      <div class="row prod-faci bg-gainsboro flex-column">
        <div class="prod-faci-title text-uppercase color-green-light font-18 pl-3">
          CƠ SỞ SẢN XUẤT KINH DOANH
        </div>
        <div class="prod-faci-list">
          <?php 
            foreach($brands as $key) {
              ?>
               <div class="prod-faci-item pt-3 pr-3 pb-3 mb-3 pl-0">
            <div class="d-flex pb-2">
              <img class="w-50" src="./img/<?=$key->logo?>" >
              <div class="w-50 pl-3 font-16 font-weight-bold d-flex align-items-center flex-wrap align-content-center">
               <?=$key->nameBrand?>
              </div>
            </div>
            <div class="color-gray-sky">
              Địa chỉ: <?=$key->address?> <br/>
              Giám đốc: <?=$key->president?><br/>
              SĐT: <?=$key->phone?><br/>
              Email: <?=$key->email?>
            </div>
          </div>
              <?php
            }
          ?>
        </div>
      </div>
    </div>

    <!--  FOOTER -->
    <div class="footer">
    </div>

    <div class="fixed-section">
    </div>
  </div>
