<div class="nav-top-wrap">
    <div class="container">
      <div class="row nav-top">
        <div class="collection-wrap">
          <img class="ml-3 mr-4" src="./img/icon-menu-black.png" width="20px">
          Danh mục sản phẩm
          <div class="collection-list">
            <div class="collection-item nav-mobile d-none"><a>Trang chủ</a></div>
            <div class="collection-item nav-mobile d-none">Kết nối cung cầu</div>
            <!-- <div class="collection-item has-child nav-mobile d-none">Nguồn cung cấp
              <div class="collection-child">
                <div class="collection-child-item">
                  item 1 - 1
                </div>
                <div class="collection-child-item">
                  item 1 - 2
                </div>
                <div class="collection-child-item">
                  item 1 - 3
                </div>
              </div>
            </div>
            <div class="collection-item has-child nav-mobile d-none">Thông tin hữu ích
              <div class="collection-child">
                <div class="collection-child-item">
                  item 1 - 1
                </div>
                <div class="collection-child-item">
                  item 1 - 2
                </div>
                <div class="collection-child-item">
                  item 1 - 3
                </div>
              </div>
            </div> -->
            <div class="collection-item nav-mobile d-none">Liên hệ phản hồi</div>
            <!-- <div class="collection-item has-child">
              item 1
              <img src="./img/icon-arrow-right-black.png" width="8px">
              <div class="collection-child">
                <div class="collection-child-item">
                  item 1 - 1
                </div>
                <div class="collection-child-item">
                  item 1 - 2
                </div>
                <div class="collection-child-item">
                  item 1 - 3
                </div>
              </div>
            </div>
            <div class="collection-item">
              item 2
              <img src="./img/icon-arrow-right-black.png" width="8px">
              <div class="collection-child">
                <div class="collection-child-item">
                  item 2 - 1
                </div>
                <div class="collection-child-item">
                  item 2 - 2
                </div>
                <div class="collection-child-item">
                  item 2 - 3
                </div>
              </div>
            </div>
            <div class="collection-item">item 3</div>
            <div class="collection-item">item 4</div>
            <div class="collection-item">item 5</div>
            <div class="collection-item">item 6</div> -->
          </div>
        </div>
        <div class="nav-list d-flex">
          <div class="nav-item" data-href="?a=home">Trang chủ</div>
          <div class="nav-item" data-href="?a=connect">Kết nối cung cầu</div>
          <!-- <div class="nav-item">
            Nguồn cung cấp
            <img class="ml-1" src="./img/icon-arrow-down-black.png" width="8px">
            <div class="nav-child">
              <a href="#" class="d-block nav-child-item">Menu con 1</a>
              <a href="#" class="d-block nav-child-item">Menu con 2</a>
              <a href="#" class="d-block nav-child-item">Menu con 3</a>
            </div>
          </div> -->
          <!-- <div class="nav-item">
            Thông tin hữu ích
            <img class="ml-1" src="./img/icon-arrow-down-black.png" width="8px">
            <div class="nav-child">
              <a href="#" class="d-block nav-child-item">Menu con 1</a>
              <a href="#" class="d-block nav-child-item">Menu con 2</a>
              <a href="#" class="d-block nav-child-item">Menu con 3</a>
            </div>
          </div> -->
          <div class="nav-item" data-href="?a=lien-he">Liên hệ phản hồi</div>
        </div>
      </div>
    </div>
  </div>