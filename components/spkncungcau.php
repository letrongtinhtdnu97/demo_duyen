  <!--  KẾT NỐI CUNG CẦU -->
  <?php 
  include 'system/sp.php';
  ?>
  <div class="connect-customer-wrap">
    <div class="container">
      <div class="row connect-customer">
        <div class="w-100 d-flex bg-green-light justify-content-between color-white">
          <div class="connect-customer-title text-uppercase font-weight-bold pl-3">
            KẾT NỐI CUNG CẦU
          </div>
          <div class="d-flex align-items-center pr-1 pr-sm-5 flex-wrap">
            <a href="?a=connect" class="pl-2 pl-sm-4">
              <img src="./img/bg-cart.png" width="130" alt="Cần mua">
            </a>
            <a href="?a=connect" class="pl-2 pl-sm-4">
              <img src="./img/bg-for-sale.png" width="130" alt="Cần bán">
            </a>
            <a href="?a=connect" class="pl-2 pl-sm-4">
              <img src="./img/bg-partner.png" width="130" alt="Tìm đối tác">
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>

<div class="product-line-wrap">
    <div class="container">
      <div class="row bg-white product-line justify-content-between">
        <?php 
          foreach($sp as $key) {
            ?>
            <div class="product-line-item">
          <div class="d-flex">
            <div class="product-line-image pt-3 pb-3">
              <img src="./img/borau.png" alt="Hình ảnh sản phẩm" width="100%">
            </div>
            <div class="product-line-info d-flex flex-column justify-content-center pl-4">
              <div class="product-line-title pb-2"><?=$key->title?></div>
              <div class="product-line-author color-gray-blue">
                <img class="pb-1" src="./img/icon-user-gray.png" width="14px"> <?=$key->fullName?>
                <span class="pl-1 pr-1">|</span>
                <img class="pb-1" src="./img/icon-clock.png" width="14px"> <?=$key->created?>
                <span class="pl-1 pr-1">|</span>
                <img src="./img/icon-menu-gray.png" width="14px"> <?php 
                  switch ($key->status) {
                    case 1:
                      # code...
                      echo "Cần mua";
                      break;
                      case 2:
                        # code...
                        echo "Cần bán";
                        break;
                        case 3:
                          echo "Tìm đối tác";
                          # code...
                          break;
                    default:
                    echo "Cần mua";
                      # code...
                      break;
                  }
                ?>
              </div>
              <div class="product-line-price pt-2"><?=$key->price?>đ/kg</div>
            </div>
          </div>
        </div>
            <?php 
          }
        ?>
    
      </div>
    </div>
  </div>