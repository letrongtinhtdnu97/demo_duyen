  <!--  FOOTER -->
  
  <div class="footer-wrap bg-white">
    <div class="container">
      <div class="row footer">
        <div class="col-12 col-md-6 col-xl-4 p-0 mb-3">
          <div class="fb-page w-100" data-href="https://www.facebook.com/nongsanantoanthanhhoa.vn/"
               data-width="500px" data-height="200" data-small-header="true" data-adapt-container-width="true"
               data-hide-cover="false" data-show-facepile="true">
            <blockquote cite="https://www.facebook.com/nongsanantoanthanhhoa.vn/" class="fb-xfbml-parse-ignore"><a
              href="https://www.facebook.com/nongsanantoanthanhhoa.vn/">Cổng thông tin kết nối nông sản an toàn tỉnh
              Thanh Hoá</a></blockquote>
          </div>
        </div>
        <div class="col-12 col-md-6 col-xl-2 mb-3">
          <div class="footer-title">THÔNG TIN</div>
          <div class="footer-menu">
            <a href="#" class="footer-item">Giới thiệu</a>
            <a href="#" class="footer-item">Quy chế hoạt động</a>
            <a href="#" class="footer-item">Điều khoản tham gia</a>
            <a href="#" class="footer-item">Chính sách bảo mật</a>
            <a href="#" class="footer-item">Hướng dẫn sử dụng</a>
          </div>
        </div>
        <div class="col-12 col-md-6 col-xl-3 mb-3">
          <div class="footer-title">CƠ CHẾ QUẢN LÝ</div>
          <div class="footer-menu">
            <a href="#" class="footer-item">Liên Minh HTX tỉnh Khánh Hòa</a>
            <a href="#" class="footer-item">HTX UBND các huyện, thành, thị</a>
            <a href="#" class="footer-item">UBND xã, phường, thị trấn</a>
            <a href="#" class="footer-item">Thông tin liên hệ</a>
            <a href="#" class="footer-item">Giải đáp thắc mắc</a>
          </div>
        </div>
        <div class="col-12 col-md-6 col-xl-3 mb-3">
          <div class="footer-title">THAM GIA CÙNG CHÚNG TÔI</div>
          <div class="footer-menu">
            <a href="#" class="footer-item">Cơ sở sản xuất kinh doanh</a>
            <a href="#" class="footer-item">Người tiêu dùng</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal-custom modal-signin login-page">
  <div class="modal-custom-form form pt-0">
    <img class="close-popup" src="./img/icon-close-orange.png" width="32px">
    <div class="tab-tab bg-gray-sky d-flex mb-5">
      <div class="modal-message signup w-50 tab-item">ĐĂNG KÝ</div>
      <div class="modal-message signin w-50 tab-item">ĐĂNG NHẬP</div>
    </div>
    <?php 
        include 'create.php';
    ?>
    <?php 
        include 'login.php';
    ?>
    
  </div>
</div>
<script src="./js/jquery-3.5.1.min.js"></script>
<script src="./js/owl.carousel.min.js"></script>
<script src="./js/popper.min.js"></script>
<script src="./js/bootstrap.min.js"></script>
<!--my script -->
<script async defer crossorigin="anonymous"
        src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v8.0&appId=305415700445910&autoLogAppEvents=1"
        nonce="0kTfuLhJ"></script>
<script src="./js/script.js"></script>

</body>
</html>