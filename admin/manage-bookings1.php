<?php
session_start();
error_reporting(0);
include('includes/config.php');
if(strlen($_SESSION['alogin'])==0)
	{	
header('location:index.php');
}
else{
if(isset($_REQUEST['eid']))
	{
$eid=intval($_GET['eid']);

$sql = "UPDATE tblbooking SET status='3' WHERE  id=:eid";
$query = $dbh->prepare($sql);
$query-> bindParam(':eid',$eid, PDO::PARAM_STR);
$query -> execute();

$msg="Hủy thành công";
}


if(isset($_REQUEST['aeid']))
	{
$aeid=intval($_GET['aeid']);


$sql = "UPDATE tblbooking SET status=2 WHERE  id=:aeid";
$query = $dbh->prepare($sql);
$query-> bindParam(':aeid',$aeid, PDO::PARAM_STR);
$query -> execute();

$msg="Xác nhận thành công";
}


 ?>

<!doctype html>
<html lang="en" class="no-js">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="theme-color" content="#3e454c">
	
	<title>Admin   </title>

	<!-- Font awesome -->
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<!-- Sandstone Bootstrap CSS -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<!-- Bootstrap Datatables -->
	<link rel="stylesheet" href="css/dataTables.bootstrap.min.css">
	<!-- Bootstrap social button library -->
	<link rel="stylesheet" href="css/bootstrap-social.css">
	<!-- Bootstrap select -->
	<link rel="stylesheet" href="css/bootstrap-select.css">
	<!-- Bootstrap file input -->
	<link rel="stylesheet" href="css/fileinput.min.css">
	<!-- Awesome Bootstrap checkbox -->
	<link rel="stylesheet" href="css/awesome-bootstrap-checkbox.css">
	<!-- Admin Stye -->
	<link rel="stylesheet" href="css/style.css">
  <style>
		.errorWrap {
    padding: 10px;
    margin: 0 0 20px 0;
    background: #fff;
    border-left: 4px solid #dd3d36;
    -webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
    box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
}
.succWrap{
    padding: 10px;
    margin: 0 0 20px 0;
    background: #fff;
    border-left: 4px solid #5cb85c;
    -webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
    box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
}
		</style>

</head>

<body>
	<?php include('includes/header.php');?>

	<div class="ts-main-content">
		<?php include('includes/leftbar.php');?>
		<div class="content-wrapper">
			<div class="container-fluid">

				<div class="row">
					<div class="col-md-12">

						<h2 class="page-title">Đơn hàng thuê xe</h2>

						<!-- Zero Configuration Table -->
						<div class="panel panel-default">
							<div class="panel-heading">Thuê xe</div>
							<div class="panel-body">
							<?php if($error){?><div class="errorWrap"><strong>ERROR</strong>:<?php echo htmlentities($error); ?> </div><?php } 
				else if($msg){?><div class="succWrap"><strong>SUCCESS</strong>:<?php echo htmlentities($msg); ?> </div><?php }?>
								<table id="zctb" class="display table table-striped table-bordered table-hover" cellspacing="0" width="100%">
									<thead>
										<tr>
										<th>#</th>
											<th>Khách Hàng</th>
											<th>Số điện thoại</th>
											<th>Mail</th>
                                            <th>Địa chỉ</th>
											<th>Xe thuê</th>
											<th>Giá tiền</th>
											<th>Ngày thuê</th>
											<th>Thời gian thuê(Ngày)</th>
                                            <th>Tổng tiền</th>
                                            <th>Trang thái</th>
											<th>Action</th>

										</tr>
									</thead>
									<tfoot>
										<tr>
										<th>#</th>
										<th>Khách Hàng</th>
                                        <th>Số điện thoại</th>
											<th>Mail</th>
                                            <th>Địa chỉ</th>
											<th>Xe thuê</th>
											<th>Giá tiền</th>
											<th>Ngày thuê</th>
											<th>Thời gian thuê(Ngày)</th>
                                            <th>Tổng tiền</th>
                                            <th>Trang thái</th>
											<th>Action</th>
										</tr>
									</tfoot>
									<tbody>

                                    <?php $sql = "SELECT A.ToDate, A.create_at,A.id,A.status,B.FullName,B.ContactNo,B.EmailId, B.Address, C.price, C.driver, D.VehiclesTitle
                                    FROM tblbooking as  A, tblusers as B, tblvehicles_rent as C,tblvehicles as D  
                                    WHERE A.id_car = D.id 
                                    and A.id_car = C.id_car
                                    and A.id_user = B.id;
                                    ";
$query = $dbh -> prepare($sql);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);
$cnt=1;
if($query->rowCount() > 0)
{
    //hãy xác nhận 0
    //chờ liên hệ 1
    //xác nhận thành công 2
foreach($results as $result)
{				?>	
										<tr>
											<td><?php echo htmlentities($cnt);?></td>
											<td><?php echo htmlentities($result->FullName);?></td>
											<td><?php echo htmlentities($result->ContactNo);?></td>
											<td><?php echo htmlentities($result->EmailId);?></td>
											<td><?php echo htmlentities($result->Address);?></td>
											<td><?php echo htmlentities($result->VehiclesTitle);?></td>
                                            <td><?php echo htmlentities($result->price);?></td>
											<td><?php echo htmlentities($result->create_at);?></td>
											<td><?php echo htmlentities($result->ToDate);?></td>
                                            <td><?php echo $result->price*$result->ToDate;?></td>
                                            <td><?php 
                                                if($result->status == 0) {
                                                    echo "Xác nhận từ khách";
                                                }
                                                if($result->status == 1) {
                                                    echo "Xác nhận bên admin";
                                                }
                                                if($result->status == 2) {
                                                    echo "Xác nhận thành công";
                                                }
                                                if($result->status == 3) {
                                                    echo "Đơn hàng đã hủy";
                                                }
                                            ;?></td>
										<td><a href="manage-bookings1.php?aeid=<?php echo htmlentities($result->id);?>" onclick="return confirm('Bạn có chắc chắn!')">Xác nhận</a> /
<a href="manage-bookings1.php?eid=<?php echo htmlentities($result->id);?>" onclick="return confirm('Bạn có muốn xóa!')"> Xóa</a>
</td>

										</tr>
										<?php $cnt=$cnt+1; }} ?>
										
									</tbody>
								</table>

						

							</div>
						</div>

					

					</div>
				</div>

			</div>
		</div>
	</div>

	<!-- Loading Scripts -->
	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap-select.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.dataTables.min.js"></script>
	<script src="js/dataTables.bootstrap.min.js"></script>
	<script src="js/Chart.min.js"></script>
	<script src="js/fileinput.js"></script>
	<script src="js/chartData.js"></script>
	<script src="js/main.js"></script>
</body>
</html>
<?php } ?>
