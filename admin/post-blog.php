<?php
session_start();
error_reporting(0);
include('includes/config.php');
if(strlen($_SESSION['alogin'])==0)
	{	
header('location:index.php');
}
else{ 

if(isset($_POST['submit']))
  {
    //   echo "<pre>";
    //   print_r($_FILES['img']['name']);
    //   echo "</pre>";
    //   exit;
$title=$_POST['title'];
$content=$_POST['content'];
$content_sub=$_POST['content_sub'];
$tag=$_POST['tag'];
$image="assets/img/blog/".$_FILES['img']['name'];


$id_user = '1';
 


$sql="INSERT INTO `tblblog`( `title`, `content`, `content_sub`, `status`, `id_user`, `rating`, `tag`, `image`) 
VALUES (:title,:content1,:content_sub1,'0',:id_user1,'5',:tag1,:image1)";

$query = $dbh->prepare($sql);
$query->bindParam(':title',$title,PDO::PARAM_STR);

$query->bindParam(':content1',$content,PDO::PARAM_STR);
$query->bindParam(':content_sub1',$content_sub,PDO::PARAM_STR);
$query->bindParam(':id_user1',$id_user,PDO::PARAM_STR);
$query->bindParam(':tag1',$tag,PDO::PARAM_STR);
$query->bindParam(':image1',$image,PDO::PARAM_STR);


$query->execute();
$lastInsertId = $dbh->lastInsertId();
if($lastInsertId)
{
$msg="Up blog successfully";
}
else 
{
$error="Something went wrong. Please try again";
}

}


	?>
<!doctype html>
<html lang="en" class="no-js">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="theme-color" content="#3e454c">
	
	<title>Admin Cars</title>

	<!-- Font awesome -->
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<!-- Sandstone Bootstrap CSS -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<!-- Bootstrap Datatables -->
	<link rel="stylesheet" href="css/dataTables.bootstrap.min.css">
	<!-- Bootstrap social button library -->
	<link rel="stylesheet" href="css/bootstrap-social.css">
	<!-- Bootstrap select -->
	<link rel="stylesheet" href="css/bootstrap-select.css">
	<!-- Bootstrap file input -->
	<link rel="stylesheet" href="css/fileinput.min.css">
	<!-- Awesome Bootstrap checkbox -->
	<link rel="stylesheet" href="css/awesome-bootstrap-checkbox.css">
	<!-- Admin Stye -->
	<link rel="stylesheet" href="css/style.css">
<style>
		.errorWrap {
    padding: 10px;
    margin: 0 0 20px 0;
    background: #fff;
    border-left: 4px solid #dd3d36;
    -webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
    box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
}
.succWrap{
    padding: 10px;
    margin: 0 0 20px 0;
    background: #fff;
    border-left: 4px solid #5cb85c;
    -webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
    box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
}
		</style>

</head>

<body>
	<?php include('includes/header.php');?>
	<div class="ts-main-content">
	<?php include('includes/leftbar.php');?>
		<div class="content-wrapper">
			<div class="container-fluid">

				<div class="row">
					<div class="col-md-12">
					
						<h2 class="page-title">Tạo blog</h2>

						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-default">
									<div class="panel-heading">Basic Info</div>
<?php if($error){?><div class="errorWrap"><strong>ERROR</strong>:<?php echo htmlentities($error); ?> </div><?php } 
				else if($msg){?><div class="succWrap"><strong>SUCCESS</strong>:<?php echo htmlentities($msg); ?> </div><?php }?>

									<div class="panel-body">
<form method="post" class="form-horizontal" enctype="multipart/form-data">
<div class="form-group">
<label class="col-sm-2 control-label">Tiêu đề<span style="color:red">*</span></label>
<div class="col-sm-4">
<textarea class="form-control" name="title" rows="5" required></textarea>
</div>
</div>
<div class="hr-dashed"></div>
<div class="form-group">
<label class="col-sm-2 control-label">Tiêu đề hỗ trợ<span style="color:red">*</span></label>
<div class="col-sm-4">
<textarea class="form-control" name="content_sub" rows="5" required></textarea>
</div>
</div>											
<div class="hr-dashed"></div>
<div class="form-group">
<label class="col-sm-2 control-label">Nội dung tổng thể<span style="color:red">*</span></label>
<div class="col-sm-10">
<textarea class="form-control" name="content" rows="9" required></textarea>
</div>
</div>

<div class="form-group">
<label class="col-sm-2 control-label">Tag<span style="color:red">*</span></label>
<div class="col-sm-4">
<input type="text" name="tag" class="form-control" required>
</div>

</div>





<div class="hr-dashed"></div>


<div class="form-group">
<div class="col-sm-12">
<h4><b>Upload Ảnh xe</b></h4>
</div>
</div>


<div class="form-group">
<div class="col-sm-4">
Click chọn <span style="color:red">*</span>
<input type="file"  required name="img" />
</div>

</div>



<div class="hr-dashed"></div>									
</div>
</div>
</div>
</div>
							
</div>
</div>




											<div class="form-group">
												<div class="col-sm-8 col-sm-offset-2">
													<button class="btn btn-default" type="reset">Cancel</button>
													<button class="btn btn-primary" name="submit" type="submit">Save changes</button>
												</div>
											</div>

										</form>
									</div>
								</div>
							</div>
						</div>
						
					

					</div>
				</div>
				
			

			</div>
		</div>
	</div>

	<!-- Loading Scripts -->
	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap-select.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.dataTables.min.js"></script>
	<script src="js/dataTables.bootstrap.min.js"></script>
	<script src="js/Chart.min.js"></script>
	<script src="js/fileinput.js"></script>
	<script src="js/chartData.js"></script>
	<script src="js/main.js"></script>
</body>
</html>
<?php } ?>