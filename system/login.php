<?php

session_start();
include('config.php');
$email=$_POST['username'];
$password=md5($_POST['password']);
$sql ="SELECT * FROM users WHERE userName=:username AND password=:password ";
$query= $dbh -> prepare($sql);
$query-> bindParam(':username', $email, PDO::PARAM_STR);
$query-> bindParam(':password', $password, PDO::PARAM_STR);
$query-> execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);

if($query->rowCount() > 0){
    $_SESSION['login']=$results[0]->id;
    $_SESSION['fullname']=$results[0]->fullName;
    $_SESSION['user']=$results[0];
    echo "<script>alert('Đăng nhập thành công');</script>";
    echo "<script type='text/javascript'> document.location = '../index.php'; </script>";
} else{
    echo "<script>alert('Tài khoản hoặc mật khẩu không đúng');</script>";
    echo "<script type='text/javascript'> document.location = '../index.php'; </script>";
}

?>