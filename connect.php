 <!--  CONNECT CUSTOMER -->
 <?php 
    include_once('./system/config.php');
    include_once('./system/sp.php');
    include './system/brand.php';

 ?>
 <div class="section-connect-wrap">
    <div class="container">
      <div class="row section-connect justify-content-around flex-wrap">
        <a href="#" class="connect-item">
          <div class="connect-title">Cần mua</div>
          <img class="connect-bg" src="./img/bg-buy.png">
        </a>
        <a href="#" class="connect-item">
          <div class="connect-title">Cần bán</div>
          <img class="connect-bg" src="./img/bg-sale.png">
        </a>
        <a href="#" class="connect-item">
          <div class="connect-title">Tìm đối tác</div>
          <img class="connect-bg" src="./img/bg-partner-2.png">
        </a>
      </div>
    </div>
  </div>

  <!-- MID FILTER -->
  <div class="mid-filter-wrap">
    <div class="container">
      <div class="row mid-filter bg-gainsboro-2">
        <select class="select-white">
          <option disabled selected>Chọn loại sản phẩm</option>
          <option value="all">Tất cả</option>
          <option value="egg">Trứng</option>
        </select>
        <select class="select-white">
          <option class="" disabled selected>Chọn Tỉnh/Huyện/Xã</option>
          <option value="">Hồ Chí Minh</option>
          <option value="">Hà Nội</option>
          <option value="">Đà Nẵng</option>
        </select>
        <input class="select-white" placeholder="Bạn cần tìm gì"/>
        <div class="bg-orange btn-icon-search color-white">
          Tìm kiếm
        </div>
      </div>
    </div>
  </div>

  <!--  ARTICLES -->
  <div class="articles-wrap">
    <div class="container">
      <div class="row articles">
        <div class="col-12 col-lg-8 pr-5">
          <div class="row justify-content-between align-items-center">
            <div>
              <div class="articles-title text-uppercase">
                Tin bài đăng mới nhất
              </div>
            </div>
            <!-- <a href="#" class="articles-btn bg-orange-dark">
              <img class="pr-3" src="./img/icon-pen-white.png">
              Đăng tin cung cầu
            </a> -->
          </div>
          <div class="row article-list">
            <?php 
                foreach($sp as $key) {
                    ?>
                    <div class="product-line-item w-100">
              <div class="d-flex flex-wrap flex-md-nowrap justify-content-between justify-content-between">
                <div class="d-flex">
                  <a href="#" class="product-line-image">
                    <img src="./img/borau.png" alt="Hình ảnh sản phẩm" width="100%">
                  </a>
                  <div class="product-line-info flex-grow-1 d-flex flex-column justify-content-between pl-4">
                    <a href="#" class="product-line-title"><?=$key->title?></a>
                    <div class="product-line-author color-gray-blue">
                      <img class="pb-1 mr-1" src="./img/icon-user-gray.png" width="14px"> <?=$key->fullName?> <span
                      class="pl-2 pr-2 d-none d-sm-inline-block">|</span>
                      <img class="pb-1 mr-1 ml-2 ml-sm-0" src="./img/icon-clock.png" width="14px"> <?=$key->created?> <span
                      class="pl-2 pr-2 d-none d-sm-inline-block">|</span><br class="d-block d-sm-none"/>
                      <img class="mr-1" src="./img/icon-menu-gray.png" width="14px"> Cần bán <span
                      class="pl-2 pr-2 d-none d-sm-inline-block">|</span>
                      <img class="d-none d-sm-inline-block mr-1" src="./img/icon-lcd.png" width="30px">
                    </div>
                    <div class="product-line-price"><?=$key->price?>đ/kg</div>
                  </div>
                </div>
                <!-- <div class="product-line-location d-flex align-items-start">
                  <img class="mr-2 mt-1" src="./img/icon-googlemap-gray.png" width="16px">
                  <div class="">Thị trấn Rừng Thông - Huyện Thanh Sơn - Tỉnh Khánh Hòa</div>
                </div> -->
              </div>
            </div>
                    <?php
                }
            ?>

            
          </div>
          
        </div>
        <div class="col-12 col-lg-4 pr-0">
          <div class="supplier-wrap">
            <div class="section-name">Nhà cung cấp tiêu biểu</div>
            <div class="supplier-list">
              <?php 
                foreach($brands as $key) {
                    ?>
                    <div class="supplier-item">
                <div class="supplier-image">
                  <img src="./img/<?=$key->logo?>">
                </div>
                <div class="supplier-info d-flex align-content-between">
                  <div class="mb-2"><strong><?=$key->nameBrand?></strong></div>
                  <div><strong>Địa chỉ:</strong> <?=$key->address?>
                  </div>
                </div>
              </div>
                    <?php
                }
                
              ?>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

